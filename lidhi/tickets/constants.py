full_w, full_h = (600, 450)
center_w, center_h = (508, 230)
padding = 37
title_font_size = 24
content_font_size = 19
line_spacing = 5
ORKNEY_MEDIUM_PATH = "static/fonts/orkney.medium.otf"
ORKNEY_REGULAR_PATH = "static/fonts/orkney.regular.otf"


FACEBOOK = 'FB'
TWITTER = 'TW'
INSTAGRAM = 'IN'
MEDIA_CHOICES = [
    (FACEBOOK, 'Facebook'),
    (TWITTER, 'Twitter'),
    (INSTAGRAM, 'Instagram'),
]
REVIEW = 'RV'
ACCEPTED = 'AC'
REJECTED = 'RJ'
TICKET_STATUS = [
    (REVIEW, 'In Progress'),
    (ACCEPTED, 'Accepted'),
    (REJECTED, 'Rejected'),
]
