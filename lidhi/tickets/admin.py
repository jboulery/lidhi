from django.contrib import admin

from .models import Ticket, Category, Publication

admin.site.register(Category)
admin.site.register(Publication)
admin.site.register(Ticket)
