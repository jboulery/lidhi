from io import BytesIO

from PIL import ImageFont, Image
from PIL import ImageDraw
from django.core.files.base import ContentFile

from lidhi.tickets.constants import full_w, full_h, center_h, center_w, padding, content_font_size, title_font_size, \
    ORKNEY_MEDIUM_PATH, ORKNEY_REGULAR_PATH, line_spacing
from lidhi.tickets.utils import get_text_width


class TicketDrawer:
    def __init__(self, font_color: str, bg_color: str, content_bg_color: str, title: str, content: str):
        self.content = content
        self.title = f'#{title}'
        self.content_bg_color = content_bg_color
        self.bg_color = bg_color
        self.font_color = font_color
        self.title_font = ImageFont.FreeTypeFont(ORKNEY_MEDIUM_PATH, title_font_size)
        self.content_font = ImageFont.FreeTypeFont(ORKNEY_REGULAR_PATH, content_font_size)

    def draw_header(self, draw):
        title_text_w, title_text_h = self.title_font.getsize(self.title)
        title_x = (full_w - title_text_w) / 2
        title_y = ((full_h - center_h) / 2 - title_text_h) / 2 + 1
        draw.text(
            xy=(title_x, title_y),
            text=self.title,
            fill=self.font_color,
            font=self.title_font)

    def draw_footer(self, draw):
        left_margin = 87
        top_margin = 20
        footer_x = left_margin
        footer_y = full_h - (full_h - center_h) / 2 + top_margin
        draw.text(
            xy=(footer_x, footer_y),
            text='#Lidhi',
            fill=self.font_color,
            font=self.title_font)

    def draw_body(self, draw, text: str):
        max_text_w = center_w - 2 * padding
        original_text_w = get_text_width(text, self.content_font)
        original_text_h = self.content_font.getsize(text)[1] + line_spacing
        text_x = (full_w - center_w) / 2 + padding

        if original_text_w > max_text_w:
            text_lines = []
            line = ""
            for word in text.split():
                next_line = line + f' {word}' if line else word
                text_w = self.content_font.getsize(next_line)[0]
                if text_w >= max_text_w:
                    text_lines.append(line)
                    line = word
                else:
                    line = next_line
            if line:
                text_lines.append(line)
            for i, line in enumerate(text_lines):
                text_y = (full_h - len(text_lines) * original_text_h) / 2 + i * original_text_h
                words = line.split()
                if i == len(text_lines) - 1:
                    draw.text(
                        xy=(text_x, text_y),
                        text=line,
                        fill=self.font_color,
                        font=self.content_font)
                    continue
                line_without_spaces = ''.join(words)
                total_size = self.content_font.getsize(line_without_spaces)
                space_width = (center_w - 2 * padding - total_size[0]) / (len(words) - 1.0)
                start_x = text_x
                for word in words[:-1]:
                    word_size = self.content_font.getsize(word)
                    draw.text(
                        xy=(start_x, text_y),
                        text=word,
                        fill=self.font_color,
                        font=self.content_font)
                    start_x += word_size[0] + space_width
                last_word_size = self.content_font.getsize(words[-1])
                last_word_x = text_x + center_w - 2 * padding - last_word_size[0]
                draw.text(
                    xy=(last_word_x, text_y),
                    text=words[-1],
                    fill=self.font_color,
                    font=self.content_font)
        else:
            text_x = (full_w - original_text_w) / 2
            text_y = (full_h - original_text_h) / 2
            draw.text(
                xy=(text_x, text_y),
                text=text,
                fill=self.font_color,
                font=self.content_font)

    def generate_preview(self):
        # Maj 24 Min 17
        # Left Margin 37
        # Text vertically centered

        # Fill background
        im = Image.new("RGB", (full_w, full_h), self.bg_color)
        center_im = Image.new("RGB", (center_w, center_h), self.content_bg_color)
        im.paste(center_im, ((full_w - center_w) // 2, (full_h - center_h) // 2))

        draw = ImageDraw.Draw(im)

        self.draw_header(draw)
        self.draw_body(draw, self.content)
        self.draw_footer(draw)

        # Save image
        img_io = BytesIO()
        im.save(img_io, format='PNG', quality=100, subsampling=0)
        return ContentFile(img_io.getvalue())
