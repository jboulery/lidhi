# Generated by Django 3.0.2 on 2020-02-27 22:48

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('tickets', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='category',
            name='slug',
            field=models.SlugField(blank=True),
        ),
        migrations.AlterField(
            model_name='ticket',
            name='details',
            field=models.TextField(blank=True, max_length=5000),
        ),
        migrations.AlterField(
            model_name='ticket',
            name='picture',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='tickets.Picture'),
        ),
        migrations.AlterField(
            model_name='ticket',
            name='slug',
            field=models.SlugField(blank=True),
        ),
    ]
