# Generated by Django 3.0.2 on 2020-02-27 22:31

import colorfield.fields
from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('title', models.CharField(max_length=200, unique=True)),
                ('slug', models.SlugField()),
                ('color', colorfield.fields.ColorField(default='#FFFFFF', max_length=18)),
                ('font_color', colorfield.fields.ColorField(default='#000000', max_length=18)),
            ],
        ),
        migrations.CreateModel(
            name='Picture',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.ImageField(upload_to='')),
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False)),
            ],
        ),
        migrations.CreateModel(
            name='Ticket',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('title', models.CharField(max_length=200, unique=True)),
                ('slug', models.SlugField()),
                ('text', models.CharField(max_length=500)),
                ('details', models.TextField(blank=True, default='', max_length=5000)),
                ('category', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='tickets.Category')),
                ('picture', models.OneToOneField(null=True, on_delete=django.db.models.deletion.CASCADE, to='tickets.Picture')),
            ],
        ),
    ]
