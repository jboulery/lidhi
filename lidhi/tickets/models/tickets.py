import uuid

from django.db import models
from django.utils.text import slugify

from .categories import Category
from ..constants import REJECTED, ACCEPTED, REVIEW
from ..domain.Drawer import TicketDrawer
from ...users.models import User


class Ticket(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    title = models.CharField(max_length=200, unique=True)
    slug = models.SlugField(max_length=50, blank=True)
    text = models.CharField(max_length=500)
    details = models.TextField(max_length=5000, blank=True)
    preview = models.ImageField(upload_to='tickets', null=True, blank=True)
    category = models.ForeignKey(Category, on_delete=models.PROTECT)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    approvals = models.ManyToManyField(User, related_name='approvals', blank=True)
    denials = models.ManyToManyField(User, related_name='denials', blank=True)

    class Meta:
        ordering = ('-created_at',)

    @property
    def status(self):
        if self.denials.first() is not None:
            return REJECTED
        elif self.approvals.count() > 1:
            return ACCEPTED
        return REVIEW

    @property
    def last_publication(self):
        return self.publication_set.last().created_at

    def __str__(self):
        return self.title

    def approved_by(self, user: User):
        self.denials.remove(user)
        self.approvals.add(user)

    def rejected_by(self, user: User):
        self.approvals.remove(user)
        self.denials.add(user)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        preview_drawer = TicketDrawer(
            font_color=self.category.font_color,
            bg_color=self.category.color,
            content_bg_color=self.category.content_bg_color,
            title=self.category.title,
            content=self.text
        )
        img_name = f'{self.slug}.png'
        img_content = preview_drawer.generate_preview()
        self.preview.save(img_name, img_content, save=False)
        super().save(*args, **kwargs)
