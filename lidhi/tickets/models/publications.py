import os
import uuid

import facebook
from django.db import models

from lidhi.tickets.constants import MEDIA_CHOICES, FACEBOOK
from lidhi.tickets.models import Ticket


class Publication(models.Model):
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    created_at = models.DateTimeField(auto_now_add=True)
    ticket = models.ForeignKey(to=Ticket, on_delete=models.CASCADE)
    media = models.CharField(max_length=2, choices=MEDIA_CHOICES)

    def save(self, *args, **kwargs):
        if self.media == FACEBOOK:
            graph = facebook.GraphAPI(access_token=os.getenv('FB_LIDHI_PAGE_TOKEN'))
            graph.put_photo(image=self.ticket.preview)
            super().save(*args, **kwargs)
