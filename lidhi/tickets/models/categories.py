import uuid

from colorfield.fields import ColorField
from django.db import models
from django.db.models import ImageField
from django.utils.text import slugify

from lidhi.tickets.domain.Drawer import TicketDrawer


class Category(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    title = models.CharField(max_length=200, unique=True)
    slug = models.SlugField(max_length=50, blank=True)
    color = ColorField(default='#FFFFFF')
    content_bg_color = ColorField(default='#FFFFFF')
    font_color = ColorField(default='#000000')
    preview = ImageField(upload_to='categories_samples', null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('-created_at',)
        verbose_name_plural = 'categories'

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        preview_drawer = TicketDrawer(
            font_color=self.font_color,
            bg_color=self.color,
            content_bg_color=self.content_bg_color,
            title=self.title,
            content=f"This is a very simple text for Category #{self.title}"
        )
        img_name = f'{self.slug}-preview.png'
        img_content = preview_drawer.generate_preview()
        self.preview.save(img_name, img_content, save=False)
        super().save(*args, **kwargs)
        for ticket in self.ticket_set.all():
            ticket.save()
