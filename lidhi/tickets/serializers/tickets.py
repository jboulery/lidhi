from rest_framework import serializers

from . import CategorySerializer
from ..models import Ticket


class TicketSerializer(serializers.ModelSerializer):
    category = CategorySerializer()

    class Meta:
        fields = (
            'id', 'created_at', 'title', 'text', 'details', 'category', 'status', 'approvals', 'denials', 'preview',
            'last_publication')
        model = Ticket
        lookup_field = 'slug'
