from rest_framework import serializers

from . import TicketSerializer
from ..models import Publication


class PublicationSerializer(serializers.ModelSerializer):
    ticket = TicketSerializer()

    class Meta:
        fields = ('id', 'created_at', 'media', 'ticket')
        model = Publication
