from rest_framework import serializers

from ..models import Category


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        fields = ('id', 'title', 'color', 'font_color', 'content_bg_color')
        model = Category
        # lookup_field = 'slug'
