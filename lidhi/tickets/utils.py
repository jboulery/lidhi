import os

import requests
from PIL import ImageFont


def get_text_width(text: str, font: ImageFont):
    text_width = 0
    for c in text:
        text_width += font.getsize(c)[0]
    return text_width


def update_slack(msg, img_url=None):
    data = {'text': msg}
    if img_url:
        data['attachments'] = {
            'image_url': img_url
        }
    requests.post(url=os.getenv('SLACK_CHANNEL_HOOK_URL'), json=data)
    pass
