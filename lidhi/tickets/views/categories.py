from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated

from ..models import Category
from ..serializers import CategorySerializer
# from ...users.permissions import IsUserOrReadOnly


class CategoryViewSet(viewsets.ModelViewSet):
    """
    Updates and retrieves user accounts
    """
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    permission_classes = (IsAuthenticated,)
    # lookup_field = 'slug'
