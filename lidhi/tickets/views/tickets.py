import os

from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.db import IntegrityError
from django.http import HttpResponse
from rest_framework import viewsets, status, serializers
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from ..models import Ticket, Category
from ..serializers import TicketSerializer
from ..utils import update_slack


class TicketViewSet(viewsets.ModelViewSet):
    """
    Updates and retrieves user accounts
    """
    queryset = Ticket.objects.all().select_related('category')
    serializer_class = TicketSerializer
    permission_classes = (IsAuthenticated,)

    @staticmethod
    def validate_form_data(data):
        category_id = data.get('category_id')
        try:
            Category.objects.get(pk=category_id)
        except (ObjectDoesNotExist, ValidationError):
            raise serializers.ValidationError({"detail": "Selected category is invalid."})
        if not data.get('title') or not data.get('text'):
            raise serializers.ValidationError({"detail": "Title and Text cannot be empty."})

    def create(self, request, *args, **kwargs):
        data = request.data
        TicketViewSet.validate_form_data(data)
        try:
            ticket = Ticket.objects.create(
                title=data['title'],
                text=data['text'],
                details=data['details'],
                category_id=data['category_id']
            )
        except (IntegrityError, ValidationError):
            raise serializers.ValidationError({"detail": "Ensure title is unique."})
        serializer = TicketSerializer(ticket)
        update_slack(
            msg=f":fire: *{request.user.username}* vient de créer un ticket "
                f"dans *#{ticket.category.title}* ! :fire:"
                f"\n*{ticket.title}*"
                f"\n_{ticket.text}_"
        )
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_200_OK, headers=headers)

    def update(self, request, *args, **kwargs):
        data = request.data
        TicketViewSet.validate_form_data(data)
        ticket = Ticket.objects.get(pk=data['id'])
        ticket.category_id = data['category_id']
        ticket.title = data['title']
        ticket.text = data['text']
        ticket.details = data['details']
        serializer = TicketSerializer(ticket)
        try:
            ticket.save()
        except (IntegrityError, ValidationError):
            raise serializers.ValidationError({"detail": "Ensure title is unique."})
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_200_OK, headers=headers)

    @action(detail=True, methods=['put'])
    def approve(self, request, *args, **kwargs):
        ticket = self.get_object()
        ticket.approved_by(request.user)
        serializer = TicketSerializer(ticket)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_200_OK, headers=headers)

    @action(detail=True, methods=['put'])
    def reject(self, request, *args, **kwargs):
        ticket = self.get_object()
        ticket.rejected_by(request.user)
        serializer = TicketSerializer(ticket)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_200_OK, headers=headers)

    @action(detail=True, methods=['get'])
    def download(self, request, *args, **kwargs):
        ticket = self.get_object()
        response = HttpResponse(ticket.preview, content_type="image/png")
        response['Content-Length'] = os.path.getsize(ticket.preview.file)
        response['Content-Disposition'] = "attachment; filename=%s" % ticket.preview.name
        return response
