from rest_framework import viewsets, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from lidhi.tickets.models import Publication, Ticket
from lidhi.tickets.serializers import PublicationSerializer
from lidhi.tickets.utils import update_slack


class PublicationViewSet(viewsets.ModelViewSet):
    """
    Updates and retrieves user accounts
    """
    queryset = Publication.objects.all().select_related('ticket')
    serializer_class = PublicationSerializer
    permission_classes = (IsAuthenticated,)

    def create(self, request, *args, **kwargs):
        data = request.data
        ticket_id = data.get('ticket_id')
        if ticket_id:
            ticket = Ticket.objects.get(pk=ticket_id)
            publication = Publication.objects.create(
                ticket=ticket,
                media=data.get('media')
            )
            serializer = PublicationSerializer(publication)
            update_slack(
                msg=f":rocket: *{request.user.username}* vient de publier ! :rocket:\n"
                    f"*#{ticket.category.title}* - *{ticket.title}*"
            )
            headers = self.get_success_headers(serializer.data)
            return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
        return Response(status=status.HTTP_400_BAD_REQUEST)
