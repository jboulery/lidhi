from rest_framework import routers
from .views import CategoryViewSet, TicketViewSet, PublicationViewSet

router = routers.DefaultRouter()
router.register('api/categories', CategoryViewSet, 'categories')
router.register('api/tickets', TicketViewSet, 'tickets')
router.register('api/publications', PublicationViewSet, 'tickets')

urlpatterns = router.urls
