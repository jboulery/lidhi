import React, {Component, Fragment} from "react";
import {render} from "react-dom";
import { HashRouter as Router, Route, Switch, Redirect } from "react-router-dom";

import {Provider as AlertProvider} from "react-alert";
import AlertTemplate from "react-alert-template-basic";

import Alerts from "./components/layout/Alerts";
import Login from "./containers/Login";
import {loadUser} from "./actions/auth";

import {Provider} from "react-redux";
import store from "./store";
import PrivateRoute from "./common/PrivateRoute";
import LeftDrawer from "./components/layout/Drawer";
import CategoriesList from "./components/categories/List";
import AddCategory from "./components/categories/Add";
import EditCategory from "./components/categories/Edit";
import TicketsList from "./components/tickets/List";
import AddTicket from "./components/tickets/Add";
import EditTicket from "./components/tickets/Edit";


// Alert options
const alertOptions = {
    timeout: 3000,
    position: 'bottom center'
};

class App extends Component {
    componentDidMount() {
        store.dispatch(loadUser());
    }

    render() {
        return (
            <Provider store={store}>
                <AlertProvider template={AlertTemplate} {...alertOptions}>
                    <Router>
                        <Fragment>
                            <Alerts/>
                            <LeftDrawer className="container">
                                <Switch>
                                    <Route exact path="/login" component={Login}/>
                                    <PrivateRoute exact path="/" component={TicketsList}/>
                                    <PrivateRoute exact path="/categories/" component={CategoriesList}/>
                                    <PrivateRoute exact path="/add/categories/" component={AddCategory}/>
                                    <PrivateRoute exact path="/categories/:categoryId/" component={EditCategory}/>
                                    <PrivateRoute exact path="/tickets/" component={TicketsList}/>
                                    <PrivateRoute exact path="/tickets/:ticketId/" component={EditTicket}/>
                                    <PrivateRoute exact path="/add/tickets/" component={AddTicket}/>
                                </Switch>
                            </LeftDrawer>
                        </Fragment>
                    </Router>
                </AlertProvider>
            </Provider>
        );
    }
}

export default App;

const container = document.getElementById("app");
render(<App/>, container);
