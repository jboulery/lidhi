import {
    ADD_CATEGORY, DELETE_CATEGORY, EDIT_CATEGORY, GET_CATEGORIES, GET_CATEGORY, RESET_CATEGORY_FORM,
    SET_CATEGORY_COLOR, SET_CATEGORY_CONTENT_BG_COLOR,
    SET_CATEGORY_FONT_COLOR,
    SET_CATEGORY_TITLE
} from "../actions/types/categories";

export const initialCategory = {
    color: "#FFF",
    content_bg_color: "#FFF",
    font_color: "#000",
    title: "",
};

const initialState = {
    categories: [],
    category: initialCategory,
};

export default function (state = initialState, action) {
    switch (action.type) {
        case GET_CATEGORIES:
            return {
                ...state,
                categories: action.payload
            };
        case ADD_CATEGORY:
            return {
                ...state,
                categories: [...state.categories, action.payload]
            };
        case EDIT_CATEGORY:
            return {
                ...state,
                category: action.payload
            };
        case DELETE_CATEGORY:
            return {
                ...state,
                categories: state.categories.filter(cat => cat.id !== action.payload)
            };
        case GET_CATEGORY:
            return {
                ...state,
                category: action.payload
            };
        case SET_CATEGORY_COLOR:
            return {
                ...state,
                category: {
                    ...state.category,
                    color: action.payload
                }
            };
        case SET_CATEGORY_FONT_COLOR:
            return {
                ...state,
                category: {
                    ...state.category,
                    font_color: action.payload
                }
            };
        case SET_CATEGORY_CONTENT_BG_COLOR:
            return {
                ...state,
                category: {
                    ...state.category,
                    content_bg_color: action.payload
                }
            };
        case SET_CATEGORY_TITLE:
            return {
                ...state,
                category: {
                    ...state.category,
                    title: action.payload
                }
            };
        case RESET_CATEGORY_FORM:
            return {
                ...state,
                category: initialCategory,
            };
        default:
            return state;
    }
}
