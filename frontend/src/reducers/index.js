import {combineReducers} from "redux";
import categories from './categories';
import errors from './errors';
import messages from './messages';
import auth from './auth';
import loginForm from './loginForm';
import tickets from './tickets';
import publications from './publications';

export default combineReducers({
    categories,
    errors,
    messages,
    auth,
    loginForm,
    tickets,
    publications,
});
