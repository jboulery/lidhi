import {AUTH_ERROR, LOGIN_FAILED, LOGIN_SUCCESS, LOGOUT_USER, USER_LOADED, USER_LOADING} from "../actions/types/auth";

const initialState = {
    token: localStorage.getItem('token'),
    isAuthenticated: null,
    isLoading: false,
    user: null
};

export default function (state = initialState, action) {
    switch (action.type) {
        case USER_LOADING:
            return {
                ...state,
                isLoading: true
            };
        case USER_LOADED:
            return {
                ...state,
                isLoading: false,
                user: action.payload,
                isAuthenticated: true,
            };
        case AUTH_ERROR:
        case LOGIN_FAILED:
        case LOGOUT_USER:
            localStorage.removeItem('token');
            return {
                ...state,
                isAuthenticated: false,
                token: null,
                user: null,
                isLoading: false
            };
        case LOGIN_SUCCESS:
            localStorage.setItem('token', action.payload.token);
            return {
                ...state,
                ...action.payload,
                isAuthenticated: true,
                isLoading: false
            };
        default:
            return state;
    }
}
