import {ADD_FB_PUBLICATION, PUBLISH_TICKET} from "../actions/types/publications";

const initialState = {
    publication: null,
    ticket_id: null,
    publishing: false
};

export default function(state = initialState, action) {
    switch (action.type) {
        case PUBLISH_TICKET:
            return {
                ...state,
                ticket_id: action.payload,
                publishing: true
            };
        case ADD_FB_PUBLICATION:
            return {
                publication: action.payload,
                publishing: false,
            };
        default:
            return state;
    }
}
