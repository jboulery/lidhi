import {RESET_PASSWORD_FIELD, SET_LOGIN_FORM_FIELD} from "../actions/types/loginForm";

const initialState = {
    username: '',
    email: '',
    password: '',
    password2: ''
};

export default function (state = initialState, action) {
    switch (action.type) {
        case(SET_LOGIN_FORM_FIELD):
            return {
                ...state,
                [action.payload.name]: action.payload.value
            };
        case RESET_PASSWORD_FIELD:
            return {
                ...state,
                password: '',
                password2: ''
            };
        default:
            return state;
    }
}
