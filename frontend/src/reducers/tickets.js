import {
    ADD_TICKET,
    DELETE_TICKET,
    GET_TICKETS,
    GET_TICKET,
    RESET_TICKET_FORM,
    SET_TICKET_TEXT,
    SET_TICKET_TITLE,
    EDIT_TICKET,
    SET_TICKET_CATEGORY,
    SET_TICKET_DETAILS,
    SET_TICKET_STATUS,
    SET_TICKET_APPROVALS,
    SET_TICKET_DENIALS
} from "../actions/types/tickets";
import {REVIEW} from "../components/tickets/constants";

const initialTicket = {
    title: "",
    text: "",
    category: null,
    details: "",
    created_at: null,
    status: REVIEW,
    approvals: [],
    denials: [],
    preview: '',
    last_publication: null,
};

const initialState = {
    tickets: [],
    ticket: initialTicket,
};

export default function (state = initialState, action) {
    switch (action.type) {
        case GET_TICKETS:
            return {
                ...state,
                tickets: action.payload
            };
        case ADD_TICKET:
            return {
                ...state,
                tickets: [...state.tickets, action.payload]
            };
        case EDIT_TICKET:
            return {
                ...state,
                ticket: action.payload
            };
        case DELETE_TICKET:
            return {
                ...state,
                tickets: state.tickets.filter(cat => cat.id !== action.payload)
            };
        case GET_TICKET:
            return {
                tickets: state.tickets.map(ticket => {
                    if (ticket.id === action.payload.id) {
                        return action.payload;
                    }
                    return ticket;
                }),
                ticket: action.payload
            };
        case SET_TICKET_TEXT:
            return {
                ...state,
                ticket: {
                    ...state.ticket,
                    text: action.payload
                }
            };
        case SET_TICKET_TITLE:
            return {
                ...state,
                ticket: {
                    ...state.ticket,
                    title: action.payload
                }
            };
        case SET_TICKET_CATEGORY:
            return {
                ...state,
                ticket: {
                    ...state.ticket,
                    category: action.payload
                }
            };
        case SET_TICKET_DETAILS:
            return {
                ...state,
                ticket: {
                    ...state.ticket,
                    details: action.payload
                }
            };
        case SET_TICKET_STATUS:
            return {
                ...state,
                ticket: {
                    ...state.ticket,
                    status: action.payload
                }
            };
        case SET_TICKET_APPROVALS:
            return {
                ...state,
                ticket: {
                    ...state.ticket,
                    approvals: action.payload
                }
            };
        case SET_TICKET_DENIALS:
            return {
                ...state,
                ticket: {
                    ...state.ticket,
                    denials: action.payload
                }
            };
        case RESET_TICKET_FORM:
            return {
                ...state,
                ticket: initialTicket,
            };
        default:
            return state;
    }
}
