export const getTicketCreatedAt = ISODateString => {
    const date = new Date(ISODateString);
    const now = new Date();
    const timeDiffInMs = now - date;
    const timeDiffInHours = timeDiffInMs / 1000 / 3600;
    return timeDiffInHours > 8 ? date.toLocaleDateString() : date.toLocaleTimeString();
};
