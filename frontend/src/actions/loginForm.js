import {RESET_PASSWORD_FIELD, SET_LOGIN_FORM_FIELD} from "./types/loginForm";

export const setLoginFormField = (name, value) => dispatch => {
    dispatch({
        type: SET_LOGIN_FORM_FIELD,
        payload: {
            name: name,
            value: value
        }
    });
};

export const resetPasswordField = () => dispatch => {
    dispatch({
        type: RESET_PASSWORD_FIELD
    });
};
