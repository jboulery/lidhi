import axios from "axios";
import {createMessage, returnErrors} from "./messages";

import {tokenConfig} from "./auth";
import {
    ADD_CATEGORY, DELETE_CATEGORY,
    EDIT_CATEGORY, GET_CATEGORIES, GET_CATEGORY, RESET_CATEGORY_FORM,
    SET_CATEGORY_COLOR, SET_CATEGORY_CONTENT_BG_COLOR,
    SET_CATEGORY_FONT_COLOR,
    SET_CATEGORY_TITLE
} from "./types/categories";

// Get Categories
export const getCategories = () => (dispatch, getState) => {
    axios.get('/api/categories/', tokenConfig(getState))
        .then(res => {
            dispatch({
                type: GET_CATEGORIES,
                payload: res.data.results
            });
        })
        .catch(err => dispatch(returnErrors(err.response.data, err.response.status)));
};

// Get Category
export const getCategory = id => (dispatch, getState) => {
    axios.get(`/api/categories/${id}/`, tokenConfig(getState))
        .then(res => {
            dispatch({
                type: GET_CATEGORY,
                payload: res.data
            });
        })
        .catch(err => dispatch(returnErrors(err.response.data, err.response.status)));
};

// Add Categories
export const addCategory = cat => (dispatch, getState) => {
    axios.post('/api/categories/', cat, tokenConfig(getState))
        .then(res => {
            dispatch(createMessage({addCategory: "Category Added"}));
            dispatch({
                type: ADD_CATEGORY,
                payload: res.data
            });
        })
        .catch(err => dispatch(returnErrors(err.response.data, err.response.status)));
};

// Edit Category
export const editCategory = (id, cat) => (dispatch, getState) => {
    axios.patch(`/api/categories/${id}/`, cat, tokenConfig(getState))
        .then(res => {
            dispatch(createMessage({editCategory: "Category Updated"}));
            dispatch({
                type: EDIT_CATEGORY,
                payload: res.data
            });
        })
        .catch(err => dispatch(returnErrors(err.response.data, err.response.status)));
};

// Delete Categories
export const deleteCategory = id => (dispatch, getState) => {
    axios.delete(`/api/categories/${id}/`, tokenConfig(getState))
        .then(res => {
            dispatch(createMessage({deleteCategory: "Category Deleted"}));
            dispatch({
                type: DELETE_CATEGORY,
                payload: id
            });
        })
        .catch(err => console.log(err));
};
export const setCategoryColor = color => ({
    type: SET_CATEGORY_COLOR,
    payload: color,
});
export const setCategoryFontColor = color => ({
    type: SET_CATEGORY_FONT_COLOR,
    payload: color,
});
export const setCategoryContentBgColor = color => ({
    type: SET_CATEGORY_CONTENT_BG_COLOR,
    payload: color,
});
export const setCategoryTitle = title => ({
    type: SET_CATEGORY_TITLE,
    payload: title,
});
export const resetCategory = () => ({
    type: RESET_CATEGORY_FORM,
});
