import axios from "axios";
import {createMessage, returnErrors} from "./messages";

import {tokenConfig} from "./auth";
import {
    ADD_TICKET, DELETE_TICKET,
    EDIT_TICKET, GET_TICKETS, GET_TICKET, RESET_TICKET_FORM,
    SET_TICKET_COLOR,
    SET_TICKET_FONT_COLOR,
    SET_TICKET_TITLE,
    SET_TICKET_TEXT, SET_TICKET_CATEGORY, SET_TICKET_DETAILS
} from "./types/tickets";

// Get Tickets
export const getTickets = () => (dispatch, getState) => {
    axios.get('/api/tickets/', tokenConfig(getState))
        .then(res => {
            dispatch({
                type: GET_TICKETS,
                payload: res.data.results
            });
        })
        .catch(err => dispatch(returnErrors(err.response.data, err.response.status)));
};

// Get Ticket
export const getTicket = id => (dispatch, getState) => {
    axios.get(`/api/tickets/${id}/`, tokenConfig(getState))
        .then(res => {
            dispatch({
                type: GET_TICKET,
                payload: res.data
            });
        })
        .catch(err => dispatch(returnErrors(err.response.data, err.response.status)));
};

// Add Tickets
export const addTicket = t => (dispatch, getState) => {
    const ticket = {
        ...t,
        category_id: t.category ? t.category.id : ""
    };
    axios.post('/api/tickets/', ticket, tokenConfig(getState))
        .then(res => {
            dispatch(createMessage({addTicket: "Ticket Added"}));
            dispatch({
                type: ADD_TICKET,
                payload: res.data
            });
        })
        .catch(err => dispatch(returnErrors(err.response.data, err.response.status)));
};

// Edit Ticket
export const editTicket = (id, t) => (dispatch, getState) => {
    const ticket = {
        ...t,
        category_id: t.category ? t.category.id : ""
    };
    axios.patch(`/api/tickets/${id}/`, ticket, tokenConfig(getState))
        .then(res => {
            dispatch(createMessage({editTicket: "Ticket Updated"}));
            dispatch({
                type: EDIT_TICKET,
                payload: res.data
            });
        })
        .catch(err => dispatch(returnErrors(err.response.data, err.response.status)));
};

// Delete Tickets
export const deleteTicket = id => (dispatch, getState) => {
    axios.delete(`/api/tickets/${id}/`, tokenConfig(getState))
        .then(res => {
            dispatch(createMessage({deleteTicket: "Ticket Deleted"}));
            dispatch({
                type: DELETE_TICKET,
                payload: id
            });
        })
        .catch(err => console.log(err));
};

// Approve Ticket
export const approveTicket = id => (dispatch, getState) => {
    axios.put(`/api/tickets/${id}/approve/`, null, tokenConfig(getState))
        .then(res => {
            dispatch(createMessage({approveTicket: "Ticket Approved"}));
            dispatch({
                type: GET_TICKET,
                payload: res.data
            })
        })
        .catch(err => dispatch(returnErrors(err.response.data, err.response.status)));
};

// Reject Ticket
export const rejectTicket = id => (dispatch, getState) => {
    axios.put(`/api/tickets/${id}/reject/`, null, tokenConfig(getState))
        .then(res => {
            dispatch(createMessage({rejectTicket: "Ticket Rejected"}));
            dispatch({
                type: GET_TICKET,
                payload: res.data
            })
        })
        .catch(err => dispatch(returnErrors(err.response.data, err.response.status)));
};

export const setTicketColor = color => ({
    type: SET_TICKET_COLOR,
    payload: color,
});
export const setTicketFontColor = color => ({
    type: SET_TICKET_FONT_COLOR,
    payload: color,
});
export const setTicketTitle = title => ({
    type: SET_TICKET_TITLE,
    payload: title,
});
export const setTicketText = text => ({
    type: SET_TICKET_TEXT,
    payload: text,
});
export const setTicketCategory = cat => ({
    type: SET_TICKET_CATEGORY,
    payload: cat,
});
export const setTicketDetails = details => ({
    type: SET_TICKET_DETAILS,
    payload: details,
});
export const resetTicket = () => ({
    type: RESET_TICKET_FORM,
});
