import axios from "axios";
import {tokenConfig} from "./auth";
import {createMessage, returnErrors} from "./messages";
import {ADD_FB_PUBLICATION, PUBLISH_TICKET} from "./types/publications";
import {GET_TICKET} from "./types/tickets";

export const addFbPublication = ticket => (dispatch, getState) => {
    dispatch({type: PUBLISH_TICKET, payload: ticket.id});
    const pub = {
        media: 'FB',
        ticket_id: ticket.id
    };
    axios.post('/api/publications/', pub, tokenConfig(getState))
        .then(res => {
            dispatch(createMessage({addPublication: `Ticket ${ticket.title} published`}));
            dispatch({
                type: ADD_FB_PUBLICATION,
                payload: res.data
            });
            dispatch({
                type: GET_TICKET,
                payload: res.data.ticket
            })
        })
        .catch(err => dispatch(returnErrors(err.response.data, err.response.status)));
};
