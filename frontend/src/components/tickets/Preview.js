import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import Typography from '@material-ui/core/Typography';
import './Preview.css';
import {contentFontSize, titleFontSize} from "./constants";

const useStyles = (color, font_color, content_bg_color) => makeStyles({
    root: {
        width: 600,
        height: 450,
        backgroundColor: color,
    },
    headerContainer: {
        width: "100%",
        height: 110,
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
    },
    title: {
        fontSize: titleFontSize,
        color: font_color,
        fontFamily: "OrkneyMedium",
    },
    bodyContainer: {
        display: "flex",
        alignItems: "center",
        height: 230,
        width: 508,
        margin: "auto",
        backgroundColor: content_bg_color,
    },
    body: {
        fontFamily: 'OrkneyRegular',
        fontSize: contentFontSize,
        marginLeft: 37,
        marginRight: 37,
        color: font_color,
    },
    footer: {
        fontSize: titleFontSize,
        fontFamily: 'OrkneyMedium',
        height: "10%",
        color: font_color,
        marginTop: 16,
        marginLeft: 87
    },
});

const Preview = props => {
    const {color, font_color, content_bg_color, text, title} = props;
    const classes = useStyles(color, font_color, content_bg_color)();

    return (
        <Card className={classes.root} variant="outlined" onClick={props.onClick?? null} style={props.style?? null}>
            <div className={classes.headerContainer}>
                <Typography align="center" className={classes.title} color="textSecondary">
                    #{title}
                </Typography>
            </div>
            <div className={classes.bodyContainer}>
                <Typography align="justify" display="block" className={classes.body} variant="body1"
                            component="div">
                    {text}
                </Typography>
            </div>
            <div>
                <Typography className={classes.footer} variant="body2" component="div">
                    #Lidhi
                </Typography>
            </div>
        </Card>
    );
};

export default Preview;
