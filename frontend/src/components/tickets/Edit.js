import React from 'react';
import TicketForm from "./Form";
import { connect } from 'react-redux';
import {getTicket, editTicket} from "../../actions/tickets";

class EditTicket extends React.Component {
    componentDidMount() {
        const { getTicket, match } = this.props;
        getTicket(match.params.ticketId);
    }

    render() {
        const { editTicket, match } = this.props;
        const ticketId = match.params.ticketId;
        return (<TicketForm
            key="edit-ticket-form"
            formTitle="Edit Ticket"
            onFormSubmit={ ticket => editTicket(ticketId, ticket) }
        />);
    }
}

const mapStateToProps = state => ({
    ticket: state.tickets.ticket,
});

export default connect(mapStateToProps, { getTicket, editTicket })(EditTicket);
