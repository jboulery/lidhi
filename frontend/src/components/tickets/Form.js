import React, {Component} from 'react';
import {connect} from "react-redux";
import PropTypes from "prop-types";
import {
    resetTicket,
    setTicketTitle,
    setTicketText, setTicketCategory, setTicketDetails,
} from "../../actions/tickets";
import Preview from "../tickets/Preview";
import {getCategories} from "../../actions/categories";
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';


class TicketForm extends Component {

    static propTypes = {
        onFormSubmit: PropTypes.func.isRequired,
        ticket: PropTypes.object.isRequired,
    };

    onSubmit = e => {
        e.preventDefault();
        const {ticket, onFormSubmit} = this.props;
        onFormSubmit(ticket);
    };

    componentWillUnmount() {
        this.props.resetTicket();
    }

    componentDidMount() {
        const {categories, getCategories} = this.props;
        if (categories.length === 0) {
            getCategories();
        }
    }

    render() {
        const {ticket, formTitle, setTicketTitle, setTicketCategory, setTicketText, setTicketDetails, categories} = this.props;
        const ticketCategory = ticket.category
            ? categories.length > 0
                ? categories.find(cat => cat.id === ticket.category.id)
                : null
            : null;
        return (
            <div className="card card-body mt-4 mb-4">
                <h2>{formTitle}</h2>
                <form onSubmit={this.onSubmit}>
                    <div className="form-group" key="ticket-title-form-group">
                        <label>Title</label>
                        <input
                            className="form-control"
                            type="text"
                            name="title"
                            onChange={e => setTicketTitle(e.target.value)}
                            value={ticket.title}
                        />
                    </div>
                    <div className="form-group" key="ticket-text-form-group">
                        <label>Text</label>
                        <textarea
                            className="form-control"
                            name="text"
                            onChange={e => setTicketText(e.target.value)}
                            value={ticket.text}
                        />
                    </div>
                    <div className="form-group" key="ticket-category-form-group">
                        <label>Category</label>
                        <FormControl style={{width: "100%"}}>
                            <Select
                                labelId="category-select-label"
                                id="category-select"
                                value={ticketCategory}
                                onChange={e => setTicketCategory(e.target.value)}
                                displayEmpty={!!ticketCategory}
                            >
                                {categories.map(cat => <MenuItem value={cat}>{cat.title}</MenuItem>)}
                            </Select>
                        </FormControl>
                    </div>
                    <div className="form-group" key="ticket-preview-form-group">
                        <label>Preview</label>
                        {ticket.category
                            ? <Preview title={ticket.category.title}
                                       color={ticket.category.color}
                                       content_bg_color={ticket.category.content_bg_color}
                                       font_color={ticket.category.font_color}
                                       text={ticket.text}
                                       onClick={ticket.preview ? () => window.open(ticket.preview) : null}
                                       style={{cursor: `${ticket.preview ? "pointer" : "default"}`}}
                            />
                            : <div style={{textAlign: 'center'}}>Select a Category first</div>
                        }
                    </div>
                    <div className="form-group" key="ticket-details-form-group">
                        <label>Details</label>
                        <textarea
                            className="form-control"
                            name="details"
                            onChange={e => setTicketDetails(e.target.value)}
                            value={ticket.details}
                        />
                    </div>
                    <div className="form-group">
                        <button type="submit" className="btn btn-primary">
                            Submit
                        </button>
                    </div>
                </form>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    ticket: state.tickets.ticket,
    categories: state.categories.categories
});

export default connect(mapStateToProps, {
    resetTicket,
    setTicketTitle,
    setTicketText,
    setTicketCategory,
    setTicketDetails,
    getCategories
})(TicketForm);
