export const REVIEW = "RV";
export const ACCEPTED = "AC";
export const REJECTED = "RJ";
export const titleFontSize = 24;
export const contentFontSize = 17;
