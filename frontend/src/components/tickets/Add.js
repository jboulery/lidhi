import React from 'react';
import TicketForm from "./Form";
import { connect } from 'react-redux';
import {addTicket} from "../../actions/tickets";

const AddTicket = props => (
    <TicketForm
        key="add-ticket-form"
        formTitle="Add Ticket"
        onFormSubmit={props.addTicket}
    />
);

export default connect(null, {addTicket})(AddTicket);
