import React from 'react';
import Chip from "@material-ui/core/Chip";
import { makeStyles } from '@material-ui/core/styles';

const useStyles = (content_bg_color, font_color) => makeStyles(theme => ({
  chip: {
    backgroundColor: content_bg_color,
    color: font_color
  },
}));

const TicketCategoryChip = props => {
    const { ticket } = props;
    const classes = useStyles(ticket.category.content_bg_color, ticket.category.font_color)();
    return <Chip className={classes.chip} size="small" label={ticket.category.title} />;
};

export default TicketCategoryChip;
