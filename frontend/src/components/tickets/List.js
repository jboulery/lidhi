import React, {Component, Fragment, forwardRef} from 'react';
import {connect} from "react-redux";
import PropTypes from "prop-types";
import {approveTicket, deleteTicket, getTickets, rejectTicket} from "../../actions/tickets";
import ControlButtons from "../layout/ControlButtons";
import MaterialTable from "material-table";
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteIcon from "@material-ui/icons/Delete";
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import PublicIcon from "@material-ui/icons/Public";
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import {addFbPublication} from "../../actions/publications";
import {ACCEPTED} from "./constants";
import CircularProgress from "@material-ui/core/CircularProgress";
import ThumbUpIcon from "@material-ui/icons/ThumbUp";
import ThumbDownIcon from "@material-ui/icons/ThumbDown";
import TicketCategoryChip from "./TicketCategoryChip";
import {Link} from "react-router-dom";

const tableIcons = {
    Add: componentProps => forwardRef((props, ref) => <ControlButtons {...props} ref={ref} {...componentProps} />),
    Approve: componentProps => forwardRef((props, ref) => <Fragment><ThumbUpIcon
        fontSize="small"/>&nbsp;{componentProps.nbApprovals}</Fragment>),
    Reject: componentProps => forwardRef((props, ref) => <Fragment><ThumbDownIcon
        fontSize="small"/>&nbsp;{componentProps.nbDenials}</Fragment>),
    Check: forwardRef((props, ref) => <Check {...props} ref={ref}/>),
    Clear: forwardRef((props, ref) => <Clear {...props} ref={ref}/>),
    Delete: forwardRef((props, ref) => <DeleteIcon {...props} ref={ref} color="secondary" fontSize="small"/>),
    DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref}/>),
    Edit: forwardRef((props, ref) => <Edit {...props} ref={ref}/>),
    Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref}/>),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref}/>),
    FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref}/>),
    LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref}/>),
    NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref}/>),
    PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref}/>),
    Publish: componentProps => forwardRef((props, ref) => componentProps.isPublishingTicket
        ? <CircularProgress color="primary" size={18}/>
        : <PublicIcon color={`${!componentProps.disabled ? "primary" : ""}`} fontSize="small"/>),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref}/>),
    Search: forwardRef((props, ref) => <Search {...props} ref={ref}/>),
    SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref}/>),
    ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref}/>),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref}/>)
};

class TicketsList extends Component {
    static propTypes = {
        tickets: PropTypes.array.isRequired,
        getTickets: PropTypes.func.isRequired,
    };

    componentDidMount() {
        this.props.getTickets();
    }

    render() {
        const {tickets, addFbPublication, publishing, ticket_id, deleteTicket, user, approveTicket, rejectTicket} = this.props;
        const tickets_data = tickets.map(ticket => ({
            title: ticket.title,
            category: ticket.category.title,
            created: ticket.created_at,
            text: `${ticket.text.slice(0, 50)}${ticket.text.length > 50 ? '...' : ''}`,
            lastPublication: ticket.last_publication,
            ticket: ticket,
        }));
        return (
            <MaterialTable
                title="Tickets"
                icons={tableIcons}
                columns={[
                    {title: 'Title', field: 'title', render: rowData => <Link to={`/tickets/${rowData.ticket.id}/`}>{rowData.ticket.title}</Link>},
                    {
                        title: 'Category',
                        field: 'category',
                        render: rowData => <TicketCategoryChip ticket={rowData.ticket}/>
                    },
                    {title: 'Created', field: 'created', type: 'date'},
                    {title: 'Text', field: 'text'},
                    {title: 'Last Publication', field: 'lastPublication', type: 'date'},
                ]}
                data={tickets_data}
                actions={[
                    {
                        icon: tableIcons.Add({createLink: "/add/tickets/"}),
                        tooltip: 'Add Ticket',
                        isFreeAction: true
                    },
                    rowData => {
                        const disabled = rowData.ticket.approvals.indexOf(user.id) !== -1;
                        return ({
                            icon: tableIcons.Approve({
                                disabled: disabled,
                                nbApprovals: rowData.ticket.approvals.length
                            }),
                            disabled: disabled,
                            tooltip: `${disabled ? "You already approved" : "Approve Ticket"}`,
                            onClick: () => approveTicket(rowData.ticket.id)
                        })
                    },
                    rowData => {
                        const disabled = rowData.ticket.denials.indexOf(user.id) !== -1;
                        return ({
                            icon: tableIcons.Reject({
                                disabled: disabled,
                                nbDenials: rowData.ticket.denials.length
                            }),
                            disabled: disabled,
                            tooltip: `${disabled ? "You already rejected" : "Reject Ticket"}`,
                            onClick: () => rejectTicket(rowData.ticket.id)
                        })
                    },
                    rowData => {
                        const disabled = rowData.ticket.status !== ACCEPTED;
                        return ({
                            icon: tableIcons.Publish({
                                disabled: disabled,
                                isPublishingTicket: publishing && rowData.ticket.id === ticket_id
                            }),
                            disabled: disabled,
                            tooltip: 'Publish Ticket',
                            onClick: () => addFbPublication(rowData.ticket)
                        })
                    },
                    rowData => ({
                        icon: tableIcons.Delete,
                        tooltip: 'Delete Ticket',
                        onClick: () => deleteTicket(rowData.ticket.id)
                    }),
                ]}
                options={{
                    filtering: true,
                    actionsColumnIndex: -1,
                    pageSize: 20,
                    pageSizeOptions: [10, 20, 50]
                }}
            />
        )
    }
}

const mapStateToProps = state => ({
    user: state.auth.user,
    tickets: state.tickets.tickets,
    publishing: state.publications.publishing,
    ticket_id: state.publications.ticket_id,
});

export default connect(mapStateToProps, {
    getTickets,
    deleteTicket,
    addFbPublication,
    approveTicket,
    rejectTicket
})(TicketsList);
