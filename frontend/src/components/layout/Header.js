import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {logout} from "../../actions/auth";

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
}));

const Header = (props) => {
    const {isAuthenticated, user, logout} = props;
    const classes = useStyles();

    return (
        <Toolbar>
            <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
                <MenuIcon/>
            </IconButton>
            <Typography variant="h6" className={classes.title}>
                Lidhi
            </Typography>
            { isAuthenticated
                ? <Button color="inherit" onClick={logout}>Logout</Button>
                : <Button color="inherit"><Link to="/login">Sign Up</Link></Button>}
        </Toolbar>
    );
};

const mapStateToProps = state => ({
    isAuthenticated: state.auth.isAuthenticated,
    user: state.auth.user,
    logout: state.auth.logout
});

export default connect(mapStateToProps, { logout })(Header);
