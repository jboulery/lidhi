import React from 'react';
import {Link as RouterLink} from 'react-router-dom';
import AddBox from '@material-ui/icons/AddBox';
import IconButton from "@material-ui/core/IconButton";

const ControlButtons = props => {
    const {createLink} = props;
    const renderLink = React.useMemo(
        () => React.forwardRef((itemProps, ref) => <RouterLink to={createLink} ref={ref} {...itemProps} />),
        [createLink],
    );
    return (<IconButton color="primary" aria-label="upload picture" component={renderLink}>
        <AddBox/>
    </IconButton>)
};

export default ControlButtons;
