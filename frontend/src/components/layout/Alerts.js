import React, {Component, Fragment} from 'react';
import { withAlert } from "react-alert";
import { connect } from "react-redux";
import PropTypes from "prop-types";


class Alerts extends Component {
    static propTypes = {
        error: PropTypes.object.isRequired,
        message: PropTypes.object.isRequired
    };

    componentDidUpdate(prevProps) {
        const { error, alert, message } = this.props;
        if (error !== prevProps.error) {
            if (error.msg.title) {
                alert.error(`Title: ${error.msg.title.join()}`);
            }
            if (error.msg.color) {
                alert.error(`Color: ${error.msg.color.join()}`);
            }
            if (error.msg.font_color) {
                alert.error(`Font Color: ${error.msg.font_color.join()}`);
            }
            if (error.msg.non_field_errors) {
                alert.error(error.msg.non_field_errors.join());
            }
            if (error.msg.password) {
                alert.error(`Password: ${error.msg.password.join()}`);
            }
            if (error.msg.username) {
                alert.error(`Username: ${error.msg.username.join()}`);
            }
            if (error.msg.detail) {
                alert.error(error.msg.detail);
            }
        }

        if (message !== prevProps.message) {
            if (message.deleteCategory) {
                alert.success(message.deleteCategory);
            }
            if (message.addCategory) {
                alert.success(message.addCategory);
            }
            if (message.editCategory) {
                alert.success(message.editCategory);
            }
            if (message.deleteTicket) {
                alert.success(message.deleteTicket);
            }
            if (message.addTicket) {
                alert.success(message.addTicket);
            }
            if (message.editTicket) {
                alert.success(message.editTicket);
            }
            if (message.addPublication) {
                alert.success(message.addPublication);
            }
        }
    }

    render() {
        return <Fragment/>;
    }
}

const mapStateToProps = state => ({
    error: state.errors,
    message: state.messages
});

export default connect(mapStateToProps)(withAlert()(Alerts));
