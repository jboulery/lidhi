import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import CategoryIcon from '@material-ui/icons/Category';
import TextFieldsIcon from '@material-ui/icons/TextFields';
import Header from "./Header";
import { connect } from 'react-redux';
import {Link, withRouter} from "react-router-dom";

const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
    },
    appBarWithDrawer: {
        width: `calc(100% - ${drawerWidth}px)`,
        marginLeft: drawerWidth,
    },
    appBarWithoutDrawer: {
        width: '100%',
        marginLeft: drawerWidth,
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
    },
    drawerPaper: {
        width: drawerWidth,
    },
    toolbar: theme.mixins.toolbar,
    content: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.default,
        padding: theme.spacing(3),
    },
}));

const LeftDrawer = props => {
    const categoriesRoute = '/categories/';
    const ticketsRoute = '/tickets/';
    const currentRoute = props.location.pathname;
    const classes = useStyles();
    const { isAuthenticated } = props;

    return (
        <div className={classes.root}>
            <CssBaseline/>
            <AppBar position="fixed" className={isAuthenticated ? classes.appBarWithDrawer : classes.appBarWithoutDrawer}>
                <Header/>
            </AppBar>
            {isAuthenticated && <Drawer
                className={classes.drawer}
                variant="permanent"
                classes={{
                    paper: classes.drawerPaper,
                }}
                anchor="left"
            >
                <div className={classes.toolbar}/>
                <Divider/>
                <List>
                    <ListItem selected={currentRoute === categoriesRoute} key='Categories' button component={props => <Link to={categoriesRoute} {...props} />}>
                        <ListItemIcon><CategoryIcon/></ListItemIcon>
                        <ListItemText primary={'Categories'}/>
                    </ListItem>
                    <ListItem selected={currentRoute === ticketsRoute} key='Tickets' button component={props => <Link to={ticketsRoute} {...props} />}>
                        <ListItemIcon><TextFieldsIcon/></ListItemIcon>
                        <ListItemText primary={'Tickets'}/>
                    </ListItem>
                </List>
                <Divider/>
                <List>
                </List>
            </Drawer>}
            <main className={classes.content}>
                <div className={classes.toolbar}/>
                {props.children}
            </main>
        </div>
    );
};

const mapStateToProps = state => ({
    isAuthenticated: state.auth.isAuthenticated
});

export default connect(mapStateToProps)(withRouter(props => <LeftDrawer {...props} />));
