import React from 'react';
import CategoryForm from "./Form";
import { connect } from 'react-redux';
import {getCategory, editCategory} from "../../actions/categories";

class EditCategory extends React.Component {
    componentDidMount() {
        const { getCategory, match } = this.props;
        getCategory(match.params.categoryId);
    }

    render() {
        const { editCategory, match } = this.props;
        const categoryId = match.params.categoryId;
        return (<CategoryForm
            key="edit-category-form"
            formTitle="Edit Category"
            onFormSubmit={ cat => editCategory(categoryId, cat) }
        />);
    }
}

const mapStateToProps = state => ({
    category: state.categories.category,
});

export default connect(mapStateToProps, { getCategory, editCategory })(EditCategory);
