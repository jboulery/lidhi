import React from 'react';
import CategoryForm from "./Form";
import { connect } from 'react-redux';
import {addCategory} from "../../actions/categories";

const AddCategory = props => (
    <CategoryForm
        key="add-category-form"
        formTitle="Add Category"
        onFormSubmit={props.addCategory}
    />
);

export default connect(null, {addCategory})(AddCategory);
