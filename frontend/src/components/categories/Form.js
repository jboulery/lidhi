import React, {Component} from 'react';
import {connect} from "react-redux";
import PropTypes from "prop-types";
import {
    resetCategory,
    setCategoryTitle,
    setCategoryColor,
    setCategoryFontColor,
    setCategoryContentBgColor,
} from "../../actions/categories";
import Preview from "../tickets/Preview";
import ColorPicker from 'material-ui-color-picker';


class CategoryForm extends Component {

    static propTypes = {
        onFormSubmit: PropTypes.func.isRequired,
        category: PropTypes.object.isRequired,
    };

    onSubmit = e => {
        e.preventDefault();
        const {category, onFormSubmit} = this.props;
        onFormSubmit(category);
    };

    componentWillUnmount() {
        this.props.resetCategory();
    }

    render() {
        const {category, formTitle, setCategoryTitle, setCategoryColor, setCategoryFontColor, setCategoryContentBgColor} = this.props;
        return (
            <div className="card card-body mt-4 mb-4">
                <h2>{formTitle}</h2>
                <form onSubmit={this.onSubmit}>
                    <div className="form-group">
                        <label>Title</label>
                        <input
                            className="form-control"
                            type="text"
                            name="title"
                            onChange={e => setCategoryTitle(e.target.value)}
                            value={category.title}
                        />
                    </div>
                    <div className="form-group">
                        <label>Category Color</label>
                        <ColorPicker
                            name="color"
                            className="form-control"
                            TextFieldProps={{value: category.color}}
                            value={category.color}
                            onChange={color => setCategoryColor(color)}
                        />
                    </div>
                    <div className="form-group">
                        <label>Category Content Color</label>
                        <ColorPicker
                            name="color"
                            className="form-control"
                            TextFieldProps={{value: category.content_bg_color}}
                            value={category.content_bg_color}
                            onChange={color => setCategoryContentBgColor(color)}
                        />
                    </div>
                    <div className="form-group">
                        <label>Font Color</label>
                        <ColorPicker
                            name="font-color"
                            className="form-control"
                            TextFieldProps={{value: category.font_color}}
                            value={category.font_color}
                            onChange={color => setCategoryFontColor(color)}
                        />
                    </div>
                    <div className="form-group">
                        <label>Preview</label>
                        <Preview title={category.title}
                                 color={category.color}
                                 font_color={category.font_color}
                                 content_bg_color={category.content_bg_color}
                                 text={"This is a very simple text."}/>
                    </div>
                    <div className="form-group">
                        <button type="submit" className="btn btn-primary">
                            Submit
                        </button>
                    </div>
                </form>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    category: state.categories.category,
});

export default connect(mapStateToProps, {
    resetCategory,
    setCategoryTitle,
    setCategoryColor,
    setCategoryFontColor,
    setCategoryContentBgColor,
})(CategoryForm);
