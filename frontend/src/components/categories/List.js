import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import PropTypes from "prop-types";
import {getCategories, deleteCategory} from "../../actions/categories";
import {Link} from "react-router-dom";
import Fab from "@material-ui/core/Fab";
import AddIcon from '@material-ui/icons/Add';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import ControlButtons from "../layout/ControlButtons";

class CategoriesList extends Component {
    static propTypes = {
        categories: PropTypes.array.isRequired,
        getCategories: PropTypes.func.isRequired,
        deleteCategory: PropTypes.func.isRequired
    };

    componentDidMount() {
        this.props.getCategories();
    }

    render() {
        return (
            <Fragment>
                <ControlButtons createLink="/add/categories/"/>
                <TableContainer component={Paper}>
                    <Table aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                <TableCell>ID</TableCell>
                                <TableCell align="right">Title</TableCell>
                                <TableCell align="right">Color</TableCell>
                                <TableCell align="right">Font Color</TableCell>
                                <TableCell align="right">Actions</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {this.props.categories.map(cat =>
                                <TableRow key={cat.id}>
                                    <TableCell scope="row"
                                               component={props => <Link to={`/categories/${cat.id}/`} {...props} />}>
                                        {cat.id.slice(0, 8)}
                                    </TableCell>
                                    <TableCell align="right">{cat.title}</TableCell>
                                    <TableCell align="right">{cat.color}</TableCell>
                                    <TableCell align="right">{cat.font_color}</TableCell>
                                    <TableCell align="right">
                                        <button
                                            className="btn btn-danger btn-sm"
                                            onClick={this.props.deleteCategory.bind(this, cat.id)}>
                                            Delete
                                        </button>
                                    </TableCell>
                                </TableRow>
                            )}
                        </TableBody>
                    </Table>
                </TableContainer>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    categories: state.categories.categories
});

export default connect(mapStateToProps, {getCategories, deleteCategory})(CategoriesList);
