import React, {Component} from "react";

class TicketList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tickets: [],
      loaded: false,
      placeholder: "Loading"
    };
  }

  componentDidMount() {
    fetch("api/v1/tickets/")
      .then(response => {
        if (response.status > 400) {
          return this.setState(() => {
            return { placeholder: "Something went wrong!" };
          });
        }
        return response.json();
      })
      .then(data => {
        console.log(data);
        this.setState(() => {
          return {
            tickets: data.results,
            loaded: true
          };
        });
      });
  }

  render() {
    const { tickets } = this.state;
    return (
      <ul>
        {tickets.map(ticket => {
          return (
            <li key={ticket.id}>
              {ticket.title} - {ticket.text}
            </li>
          );
        })}
      </ul>
    );
  }
}

export default TicketList;
